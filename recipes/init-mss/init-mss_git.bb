inherit autotools-brokensep update-rc.d systemd

DESCRIPTION = "Modem init"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"
PR = "r7"

FILESPATH =+ "${WORKSPACE}/mdm-ss-mgr:"
FILESEXTRAPATHS_prepend := "${THISDIR}/init_mss:"

SRC_URI = "file://init_mss"
SRC_URI += "file://init_sys_mss.service"
SRC_URI += "file://init_mss.rules"
SRC_URI += "file://init_mss.conf"

S = "${WORKDIR}/init_mss"

# Hold /dev/subsys_modem forever on all SOCs which don't have Modem wakeup support.
EXTRA_OECONF_append_msm = " --enable-indefinite-sleep"
EXTRA_OECONF_append_sdxpoorwills = " --enable-indefinite-sleep"
EXTRA_OECONF_append_sdxprairie = " --enable-indefinite-sleep"
EXTRA_OECONF_append_qti-distro-base = " --enable-indefinite-sleep"

EXTRA_OECONF_append_qcs40x = " --enable-indefinite-sleep=yes"
EXTRA_OECONF_append_qcs40x = " --enable-wcnss=yes"

EXTRA_OECONF_append = " --enable-modem"

# QCS40x has wcnss but not modem
EXTRA_OECONF_remove_qcs40x = "--enable-modem"

FILES_${PN} += "${systemd_unitdir}/system/"
FILES_${PN} += "${sysconfdir}/udev/rules.d/"

INITSCRIPT_NAME = "init_sys_mss"
INITSCRIPT_PARAMS = "start 38 2 3 4 5 ."
INITSCRIPT_PARAMS_sdxpoorwills = "start 31 S ."
INITSCRIPT_PARAMS_sdxprairie = "start 31 S ."

do_install() {
    install -m 0755 ${S}/init_mss -D ${D}/sbin/init_mss
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
        install -d ${D}${systemd_unitdir}/system/
        install -d ${D}${sysconfdir}/udev/rules.d/
        install -m 0644 ${WORKDIR}/init_sys_mss.service -D ${D}${systemd_unitdir}/system/init_sys_mss.service
        install -m 0644 ${WORKDIR}/init_mss.rules -D ${D}${sysconfdir}/udev/rules.d/init_mss.rules
        install -m 0644 ${WORKDIR}/init_mss.conf -D ${D}${sysconfdir}/tmpfiles.d/init_mss.conf
        if ${@bb.utils.contains_any('DISTRO_NAME', 'mdm auto', 'true', 'false', d)}; then
           #ADD NAND CHECK IF REQUIRED.
           # Clear the values of After, Requires and WantedBy.
           sed -i '/After/s/firmware.mount//' ${D}${systemd_unitdir}/system/init_sys_mss.service
           sed -i '/Requires/s/firmware.mount//' ${D}${systemd_unitdir}/system/init_sys_mss.service
           sed -i '/WantedBy/s/sysinit.target//' ${D}${systemd_unitdir}/system/init_sys_mss.service

           # Add new values to After, Requires and WantedBy.
           sed -i '/\<After\>/s/$/firmware-mount.service QCMAP_ConnectionManagerd.service/' ${D}${systemd_unitdir}/system/init_sys_mss.service
           sed -i '/Requires/s/$/firmware-mount.service/' ${D}${systemd_unitdir}/system/init_sys_mss.service
           sed -i '/WantedBy/s/$/sockets.target/' ${D}${systemd_unitdir}/system/init_sys_mss.service

           # Add new lines after "Requires=firmware-mount.service" to set DefaultDependencies to no.
           sed -i '/Requires=firmware-mount.service/a DefaultDependencies=no' ${D}${systemd_unitdir}/system/init_sys_mss.service
           sed -i '/Requires=firmware-mount.service/a Before=sockets.target' ${D}${systemd_unitdir}/system/init_sys_mss.service


            # Add sleep for mdm targets to ensure full CPU is available to load modem.
            sed -i '/RemainAfterExit=yes/a ExecStartPost=+sleep 11' ${D}${systemd_unitdir}/system/init_sys_mss.service
            install -d ${D}${systemd_unitdir}/system/sockets.target.wants/
            ln -sf ${systemd_unitdir}/system/init_sys_mss.service ${D}/${systemd_unitdir}/system/sockets.target.wants/init_sys_mss.service
        else
            install -d ${D}${systemd_unitdir}/system/sysinit.target.wants
            ln -sf ${systemd_unitdir}/system/init_sys_mss.service ${D}/${systemd_unitdir}/system/sysinit.target.wants/init_sys_mss.service
        fi
    else
        install -m 0755 ${S}/start_mss -D ${D}${sysconfdir}/init.d/init_sys_mss
    fi
}
